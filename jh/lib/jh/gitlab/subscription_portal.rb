# frozen_string_literal: true

module JH
  module Gitlab
    module SubscriptionPortal
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :default_subscriptions_url
        def default_subscriptions_url
          ::Gitlab.dev_or_test_env? ? 'https://customers.stg.gitlab.com' : 'https://about.gitlab.cn/company/contact'
        end
      end
    end
  end
end
