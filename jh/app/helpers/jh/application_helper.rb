# frozen_string_literal: true

module JH
  module ApplicationHelper
    extend ActiveSupport::Concern

    class_methods do
      extend ::Gitlab::Utils::Override

      override :promo_host
      def promo_host
        'about.gitlab.cn'
      end
    end
  end
end
