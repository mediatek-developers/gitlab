# frozen_string_literal: true

module JH
  module SubmitUsagePingService
    def url
      "#{::VersionCheck.host}/usage_data"
    end
  end
end
