# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'layouts/devise.html.haml' do
  include ApplicationHelper

  before do
    render
  end

  context 'JH login page use locale zh_CN by default' do
    it 'renders Chinese description' do
      expect(rendered).to have_content('一体化安全DevOps平台')
      expect(rendered).to have_content('极狐GitLab是一个适用于整个软件开发生命周期的单一应用程序，从项目计划、源代码管理到CI/CD、监控和安全防护，加速您的DevOps生命周期。')
      expect(rendered).to have_content('这是一个在本地托管的极狐GitLab实例。')
    end
  end
end
