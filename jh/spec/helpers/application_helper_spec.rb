# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ApplicationHelper do
  describe '#promo_host' do
    it 'returns the default promo host' do
      expect(helper.promo_host).to eq('about.gitlab.cn')
    end
  end
end
